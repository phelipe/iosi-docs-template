# iosi_init_docs

Repositório com exemplo para documentação inicial do proeto IOSI.

# Utilização
Clone este repositório e dentro da pasta do projeto utilize o comando:
```bash
npm install
```
isso irá instalar todas asdependências necessárias. Após isso, para escrever a documentação é necessário entrar no modo desenvolvimento, para isto basta usar o comando a seguir.

```bash
npm run dev
```
Com isso feito já é possível criar arquivos markdown e editar os existente, estes ficam localizados na pasta arquivos, recomenda-se utilizar esta página para criar seus arquivos.


Caso deseje criar uma versão de distribuição deve-se utilizar o comando:
```bash
npm run build
```


