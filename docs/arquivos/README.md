# Introdução

Este é seu arquivo inicial


## Usando markdown

Este é um parágrafo.
  
Este é outro parágrafo.


texto modificado _aqui_!

texto modificado *aqui*!

**Aqui** outra modificação no texto!

__Aqui__ outra modificação no texto!

<hr>

[Aqui](http://www.google.com) é um exemplo de link externo.

<hr>

> Esse é um bloco de citação.
> Ele pode ter várias linhas por parágrafo.
>
> Inclusive, dando um espaço, você tem um novo parágrafo.

<hr>

Exemplos de lista
* Item 1
* Item 2
* Item 3
  
1. Item 1
1. Item 2
1. Item 3
   1. Item 3a
   1. Item 3b

* Item 1
* Item 2
  * Item 2a
  * Item 2b

<hr>
A seguir temos um exemplo de uma imagem

![Banana](http://cdn.osxdaily.com/wp-content/uploads/2013/07/dancing-banana.gif)

<hr>
Exemplo de código

```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```

<hr>

Lista de tarefas

- [x] tarefa 1
- [x] tarefa 2
- [ ] tarefa 3

<hr>
Exemplo de tabela

coluna 1 | coluna 2
-------- | --------
conteúdo 1-1 | conteúdo 1-2
conteúdo 2-1 | conteúdo 2-2


Emojis
É possível utilizar emojis :grinning:


