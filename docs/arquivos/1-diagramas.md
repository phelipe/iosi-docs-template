# Diagramas

Aqui é utilizado o mermaid para criação de diagramas, seguem alguns exemplos a seguir

```mermaid
graph TD
	A(Módulo) --> B(Liga fase)
  B --> host(Sobe host ap)
	host --> C(Aplicativo Configuração WIFI)
  C --> D(dispositivo é registrado no banco de dados e o usuário recebe endereço mac do dispositivo)
	D --> fechaapp(Desliga app de configuração wifi)
fechaapp --> ligaappuso(Liga app de uso)  
ligaappuso --> login(Loga com a conta)
login --> J(usuário usa o mac para cadastrar dispositivo na sua conta)
J --> verifica{Verifica se o dispositivo pertence ou não a empresa}
verifica --> |sim| sim(registra dispositivo na conta do usuário) 
verifica --> |não| nao(impede registro)
```
<hr>

```mermaid
sequenceDiagram
	Alice->>+John: Hello John, how are you?
	Alice->>+John: John, can you hear me?
	John-->>-Alice: Hi Alice, I can hear you!
	John-->>-Alice: I feel great!
					
```

<hr>

```mermaid
classDiagram
	Animal <|-- Duck
	Animal <|-- Fish
	Animal <|-- Zebra
	Animal : +int age
	Animal : +String gender
	Animal: +isMammal()
	Animal: +mate()
	class Duck{
		+String beakColor
		+swim()
		+quack()
	}
	class Fish{
		-int sizeInFeet
		-canEat()
	}
	class Zebra{
		+bool is_wild
		+run()
	}
					
```

<hr>

```mermaid

stateDiagram
	[*] --> Still
	Still --> [*]

	Still --> Moving
	Moving --> Still
	Moving --> Crash
	Crash --> [*]
					
```

<hr>

```mermaid
gantt
	title A Gantt Diagram
	dateFormat  YYYY-MM-DD
	section Section
	A task           :a1, 2014-01-01, 30d
	Another task     :after a1  , 20d
	section Another
	Task in sec      :2014-01-12  , 12d
	another task      : 24d
					
```
<hr>

```mermaid
pie title Pets adopted by volunteers
	"Dogs" : 386
	"Cats" : 85
	"Rats" : 15
```

<hr>

```mermaid
erDiagram
        CUSTOMER }|..|{ DELIVERY-ADDRESS : has
        CUSTOMER ||--o{ ORDER : places
        CUSTOMER ||--o{ INVOICE : "liable for"
        DELIVERY-ADDRESS ||--o{ ORDER : receives
        INVOICE ||--|{ ORDER : covers
        ORDER ||--|{ ORDER-ITEM : includes
        PRODUCT-CATEGORY ||--|{ PRODUCT : contains
        PRODUCT ||--o{ ORDER-ITEM : "ordered in"
					
```